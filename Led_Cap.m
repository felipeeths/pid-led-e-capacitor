function varargout = Led_Cap(varargin)
% LED_CAP MATLAB code for Led_Cap.fig
%      LED_CAP, by itself, creates a new LED_CAP or raises the existing
%      singleton*.
%
%      H = LED_CAP returns the handle to a new LED_CAP or the handle to
%      the existing singleton*.
%
%      LED_CAP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LED_CAP.M with the given input arguments.
%
%      LED_CAP('Property','Value',...) creates a new LED_CAP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Led_Cap_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Led_Cap_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Led_Cap

% Last Modified by GUIDE v2.5 24-Jul-2016 12:01:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Led_Cap_OpeningFcn, ...
                   'gui_OutputFcn',  @Led_Cap_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Led_Cap is made visible.
function Led_Cap_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Led_Cap (see VARARGIN)
global i;
global img_array;
i = 1;
img_array = [];
handles.image = 0;
handles.img_Original = 0;
global Quantidade_Led;
global Quantidade_Cap;
Quantidade_Led = 0;
Quantidade_Cap = 0;
set(handles.Led_Edit,'string',Quantidade_Led);
set(handles.Capacitor_Edit,'string',Quantidade_Cap);
img_array{i} = 0;  
% Choose default command line output for Led_Cap
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Led_Cap wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Led_Cap_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function arquivo_Callback(hObject, eventdata, handles)
% hObject    handle to arquivo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function arquivo_abrir_Callback(hObject, eventdata, handles)
% hObject    handle to arquivo_abrir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
i = 1;
img_array = [];
global Quantidade_Led;
global Quantidade_Cap;
Quantidade_Led = 0;
Quantidade_Cap = 0;
i = i + 1;
[path,dir] = imgetfile();
if dir
	msgbox(sprintf('Error nenhuma imagem selecionada !'),'Error','Error');
	return
end
set(handles.Led_Edit,'string',Quantidade_Led);
set(handles.Capacitor_Edit,'string',Quantidade_Cap);
image = imread(path);
img_array{i} = image;
img_Original = image;
axes(handles.Image);
imshow(img_array{i});
handles.img_Original = img_Original;
guidata(hObject, handles);

% --------------------------------------------------------------------
function arquivo_novo_Callback(hObject, eventdata, handles)
% hObject    handle to arquivo_novo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
global Quantidade_Led;
global Quantidade_Cap;
Quantidade_Led = 0;
Quantidade_Cap = 0;
img_array = [];
i = 1;
img_array{i} = 0;
set(handles.Led_Edit,'string',Quantidade_Led);
set(handles.Capacitor_Edit,'string',Quantidade_Cap);
handles.img_Original = 0;
cla(handles.Image,'reset');
guidata(hObject, handles);


% --------------------------------------------------------------------
function arquivo_salvar_Callback(hObject, eventdata, handles)
% hObject    handle to arquivo_salvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 F = getframe(handles.Image);
 Image = frame2im(F);
 [filename, ext, user_canceled] = imputfile;
 imwrite(Image,filename);
 Mensagem = {'Imagem salvada com sucesso'};
 msgbox(Mensagem,'Success','custom',Image);

% --------------------------------------------------------------------
function arquivo_fechar_Callback(hObject, eventdata, handles)
% hObject    handle to arquivo_fechar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;

% --------------------------------------------------------------------
function editar_Callback(hObject, eventdata, handles)
% hObject    handle to editar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ajuda_help_Callback(hObject, eventdata, handles)
% hObject    handle to ajuda_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ajuda_sobre_Callback(hObject, eventdata, handles)
% hObject    handle to ajuda_sobre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function editar_redo_Callback(hObject, eventdata, handles)
% hObject    handle to editar_redo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
i = i + 1;
try      
    imshow(img_array{i});
catch
    i = i - 1;
end


% --------------------------------------------------------------------
function editar_undo_Callback(hObject, eventdata, handles)
% hObject    handle to editar_undo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
i = i - 1;
try
    if img_array{i} == 0
       cla(handles.Image,'reset');
    else
      imshow(img_array{i});      
    end
catch
    i  = i + 1;
end


% --------------------------------------------------------------------
function editar_original_Callback(hObject, eventdata, handles)
% hObject    handle to editar_original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
if img_Original == 0
    cla(handles.Image,'reset');
else
    axes(handles.Image);
    imshow(img_Original);
    i = i + 1;
    img_array{i} = img_Original;
end


% --------------------------------------------------------------------
function ajuda_Callback(hObject, eventdata, handles)
% hObject    handle to ajuda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Led_Edit_Callback(hObject, eventdata, handles)
% hObject    handle to Led_Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Led_Edit as text
%        str2double(get(hObject,'String')) returns contents of Led_Edit as a double


% --- Executes during object creation, after setting all properties.
function Led_Edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Led_Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Capacitor_Edit_Callback(hObject, eventdata, handles)
% hObject    handle to Capacitor_Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Capacitor_Edit as text
%        str2double(get(hObject,'String')) returns contents of Capacitor_Edit as a double


% --- Executes during object creation, after setting all properties.
function Capacitor_Edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Capacitor_Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in StartButton.
function StartButton_Callback(hObject, eventdata, handles)
% hObject    handle to StartButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
global Quantidade_Led;
global Quantidade_Cap;
global B;
Quantidade_Led = 0;
Quantidade_Cap = 0;
if img_Original == 0
    cla(handles.Image,'reset');
else
    I = rgb2gray(img_array{i});
    level = graythresh(I);
    bw = ~im2bw(I,level);
    i = i + 1;
    img_array{i} = bw;
    se = strel('disk',9);
    bw = imclose(bw,se);
    i = i + 1;
    img_array{i} = bw;
    ce = strel('disk',15);
    bw = imerode(bw,ce);
    i = i + 1;
    img_array{i} = bw;
    bw = bwareaopen(bw,300);
    i = i + 1;
    img_array{i} = bw;

    stats = regionprops(bw,'BoundingBox');

    double areas2;

    for j = 1:length(stats)
        img = imcrop(bw,stats(j).BoundingBox);
        status = regionprops(img,'Area','Centroid');
        area = status(1).Area;
        areas2(j) = double(area);
    end


    B = sort(areas2,2);

    for j = 1:length(stats)
        img2 = imcrop(bw,stats(j).BoundingBox);
        status = regionprops(img2,'Area','Centroid');
        area = status(1).Area;
        valor = area - B(1);
        if(valor < 1900)
            %figure(i),imshow(img2); title('Led');
            Quantidade_Led = Quantidade_Led + 1;
        else
            %figure(i),imshow(img2); title('Capacitor');
            Quantidade_Cap = Quantidade_Cap + 1;
        end
    end
    set(handles.Led_Edit,'string',Quantidade_Led);
    set(handles.Capacitor_Edit,'string',Quantidade_Cap);
    axes(handles.Image);
    imshow(img_Original);
    
end

% --- Executes on button press in ImageOrig_Button.
function ImageOrig_Button_Callback(hObject, eventdata, handles)
% hObject    handle to ImageOrig_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
i = 2;
if img_Original == 0
    cla(handles.Image,'reset');
else
    imshow(img_array{i});
end

% --- Executes on button press in Binarizar_Buttom.
function Binarizar_Buttom_Callback(hObject, eventdata, handles)
% hObject    handle to Binarizar_Buttom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
i = 3;
if img_Original == 0
    cla(handles.Image,'reset');
else
    imshow(img_array{i});
end  

% --- Executes on button press in Imclose_Buttom.
function Imclose_Buttom_Callback(hObject, eventdata, handles)
% hObject    handle to Imclose_Buttom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
i = 4;
if img_Original == 0
    cla(handles.Image,'reset');
else
    imshow(img_array{i});
end  

% --- Executes on button press in Erode_Button.
function Erode_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Erode_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
i = 5;
if img_Original == 0
    cla(handles.Image,'reset');
else
    imshow(img_array{i});
end  


% --- Executes on button press in Open_Button.
function Open_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Open_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
i = 6;
if img_Original == 0
    cla(handles.Image,'reset');
else
    imshow(img_array{i});
end  

%
% --- Executes on button press in Region_Button.
function Region_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Region_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
if img_Original == 0
    cla(handles.Image,'reset');
else
    bw = img_array{i};
    [B,L] = bwboundaries(bw,'noholes');
    imshow(label2rgb(L, @jet, [.5 .5 .5]))
    hold on
    for k = 1:length(B)
      boundary = B{k};
      plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
    end
end  

% --- Executes on button press in Area_Button.
function Area_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Area_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
global B;
if img_Original == 0
    cla(handles.Image,'reset');
else
    bw = img_array{i};
    [C,L] = bwboundaries(bw,'noholes');
    imshow(label2rgb(L, @jet, [.5 .5 .5]))
    hold on
    for k = 1:length(C)
      boundary = C{k};
      plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
    end
    stats = regionprops(L,'Area','Centroid');
    for k = 1:length(C)
        boundary = C{k};
        centroid = stats(k).Centroid;
        area = stats(k).Area;
        metric_string = sprintf('%d',area);
        text(boundary(1,2)-35,boundary(1,1)+13,metric_string,'Color','y',...
       'FontSize',14,'FontWeight','bold');
    end
end  


% --- Executes on button press in Identificar_Button.
function Identificar_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Identificar_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
global B;
if img_Original == 0
    cla(handles.Image,'reset');
else
    bw = img_array{i};
    [C,L] = bwboundaries(bw,'noholes');
    imshow(label2rgb(L, @jet, [.5 .5 .5]))
    hold on
    for k = 1:length(C)
      boundary = C{k};
      plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
    end
    stats = regionprops(L,'Area','Centroid');
    for k = 1:length(C)
        boundary = C{k};
        centroid = stats(k).Centroid;
        area = stats(k).Area;
        valor = area - B(1);
        if(valor < 1900)
            led = 'Led';
            metric_string = sprintf('%s',led);
            text(boundary(1,2)-35,boundary(1,1)+13,metric_string,'Color','y',...
             'FontSize',14,'FontWeight','bold');
        else
            capacitor = 'Capacitor';
             metric_string = sprintf('%s',capacitor);
            text(boundary(1,2)-35,boundary(1,1)+13,metric_string,'Color','y',...
             'FontSize',14,'FontWeight','bold');
        end
        
    end
end  